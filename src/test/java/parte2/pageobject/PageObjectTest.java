package parte2.pageobject;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import parte2.util.BaseTest;

public class PageObjectTest extends BaseTest {

	@Test
	public void deveAcessarCampoDeTreinamento() {
		
		String tituloPagina = "";
		
		tituloPagina = driver.getTitle();
		
		System.out.println("tituloPagina: " + tituloPagina);
		
		assertEquals(tituloPagina, "Campo de Treinamento");
		
	}
	
}
