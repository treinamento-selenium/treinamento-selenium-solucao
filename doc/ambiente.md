# Doc - Ambiente (Java, Maven, Variáveis de ambiente)

O objetivo deste documento é auxiliar na configuração para um ambiente de desenvolvimento em uma máquina Windows, 
utilizando as tecnologias apresentadas a seguir.

<b>Considerações:</b>

Para facilitar e focar na didática, vamos considerar os itens abaixo como objeto de estudo e tutorial de configuração.

- Sistema Operacional: Windows 7 ou superior.
- JDK: Versão igual ou superior a 1.8.
- Navegador: Google Chrome
- Durante a execução de uma Etapa, verifique se no seu computador já existe a configuração especificada na etapa.. 

<b>Tópicos</b>

* [Java](#java)
    * [1ª Etapa - Instalação da JDK](#java-1ªetapa-instalação-da-jdk)
    * [2ª Etapa - Váriaveis de ambiente - JAVA_HOME](#java-2ª-etapa-váriaveis-de-ambiente-java_home)
    * [3ª Etapa - Váriaveis de ambiente - CLASSPATH](#java-3ª-etapa-váriaveis-de-ambiente-classpath)
    * [4ª Etapa - Váriaveis de ambiente - Path](#java-4ª-etapa-váriaveis-de-ambiente-path)

    
* [Maven](#maven)
    * [1ª Etapa - Instalando o Maven](#maven-1ªetapa-instalando-o-maven)
    * [2ª Etapa - Váriaveis de ambiente - M2_HOME](#maven-2ª-etapa-váriaveis-de-ambiente-m2_home)
    * [3ª Etapa - Váriaveis de ambiente - Path](#maven-3ª-etapa-váriaveis-de-ambiente-path)


* [Selenium Drivers](#selenium-drivers)
    * [Selenium - 1ª Etapa - Instalando o Driver](#selenium-1ª-etapa-instalando-o-driver)

# Java

## Java - 1ª Etapa - Instalação da JDK

O JDK (Java Development Kit) possui todo o ambiente necessário para desenvolver e executar aplicativos em Java, contendo uma cópia do JRE.

O JRE (Java Runtime Envirorment) é o ambiente de execução Java, é o mínimo que você precisa ter instalado para poder rodar um aplicativo Java.

[Clique aqui para ir para página de download ORACLE JDKs.](https://www.oracle.com/technetwork/pt/java/javase/downloads/index.html)

## Java - 2ª Etapa - Váriaveis de ambiente - JAVA_HOME

1 -  Abra uma pasta qualquer;

2 -  Clique com o botão direito em cima do ícone "This PC" (Figura - 1);
 
 <div align='center'>
 
 <b>Figura - 1</b>
  
 ![config.ambiente.variavel.1](doc/img/config.ambiente.variavel.1.png)
 
 </div>
 
3 - Vá em "Properties" (Figura - 2);

<div align='center'>

<b>Figura - 2</b>
 
![config.ambiente.variavel.2](doc/img/config.ambiente.variavel.2.png)

</div>

4 - Clique em "Advanced system settings" (Figura - 3);

<div align='center'>

<b>Figura - 3</b>
 
![config.ambiente.variavel.3](doc/img/config.ambiente.variavel.3.png)

</div>

5 - Clique no botão "Environment Variables..." (Figura - 4);

<div align='center'>

<b>Figura - 4</b> 

![config.ambiente.variavel.4](doc/img/config.ambiente.variavel.4.png)

</div>

6 - Clique no botão "New..." em "System variables" (Figuras - 5 e 6);
   * Variable name (Nome da variável): JAVA_HOME;
   * Variable value (Valor da variável): coloque aqui o endereço de instalação. Exemplo: C:\Arquivos de programas\Java\jdk1.5.0_05 
   * Clique em OK;

<div align='center'>

<b>Figura - 5</b> 

![config.ambiente.variavel.5](doc/img/config.ambiente.variavel.5.png) 

</div>

<div align='center'>

<b>Figura - 6</b>
 
![config.ambiente.variavel.6](doc/img/config.ambiente.variavel.6.png)

</div>

## Java - 3ª Etapa - Váriaveis de ambiente - CLASSPATH

 1. Clique novamente no botão "New..." em "System variables";
    * Variable name (Nome da variável): CLASSPATH;
    * Variable value (Valor da variável): coloque aqui o valor que encontram-se abaixo, sempre insira um ; (ponto e vírgula) no inicio e outro no final . 
    * Clique em OK;

``` 
  ;%JAVA_HOME%\lib;%JAVA_HOME%\lib\tools.jar;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\htmlconverter.jar;%JAVA_HOME%\jre\lib;%JAVA_HOME%\jre\lib\rt.jar;
```

## Java - 4ª Etapa - Váriaveis de ambiente - Path

1 - Ainda em "System variables", localize a variábel: Path;

2 - Clique no botão "Edit...";

<div align='center'>

<b>Figura - 7</b> 

![config.ambiente.variavel.7](doc/img/config.ambiente.variavel.7.png)

</div>

3 - Clique no botão "New...";
4 - Insia o valor: %JAVA_HOME%\bin
5 - Confirme todas as telas no botão OK; 

# Maven

O Maven é uma ferramenta de gerenciamento de dependências e do ciclo de vida de projetos de software no sentido técnico. Isso inclui:

* Facilitar a compilação do código, o empacotamento (JAR, WAR, EAR, …), a execução de testes unitários, etc.
* Unificar e automatizar o processo de geração do sistema. Nada mais de uma coleção de passos e scripts a serem executados manualmente.
* Centralizar informações organizadas do projeto, incluindo suas dependências, resultados de testes, documentação, etc.
* Reforçar boas práticas de desenvolvimento, tais como: separação de classes de teste das classes do sistema, uso de convenções de nomes e diretórios, etc.
* Ajuda no controle das versões geradas (releases) dos seus projetos.

## Maven - 1ª Etapa - Instalando o Maven

1 - Acesse a página do Maven e clique no item Download do menu.

2 - A página disponibiliza diferentes versões para diferentes ambientes. Baixe o arquivo da última versão de acordo com seu sistema operacional. Destaquei na imagem a versão zip para Windows que usarei neste exemplo:

<div align='center'>

<b>Figura - 8</b> 

![config.ambiente.maven.instalando.1](doc/img/config.ambiente.maven.instalando.1.png)

</div>

3 - Descompacte o conteúdo para a pasta c:\starcode\.

A versão mais atual do Maven na data de criação deste tutorial é 3.2.1. O pacote baixado é nomeado apache-maven-3.2.1-bin.zip. Veja o arquivo aberto no 7-Zip:

## Maven - 2ª Etapa - Váriaveis de ambiente - M2_HOME

Para usar o Maven em linha de comando você deve adicionar o caminho para os executáveis ao PATH do ambiente. No Windows, pressione Ctrl+Break para abrir a tela de informações do do sistema.

1 - Clique no botão "New..." em "System variables" (Figuras - 5);
   * Variable name (Nome da variável): M2_HOME;
   * Variable value (Valor da variável): coloque aqui o endereço de instalação. Exemplo: c:\starcode\apache-maven-3.2.1 
   * Clique em OK;

<div align='center'>

<b>Figura - 5</b> 

![config.ambiente.variavel.5](doc/img/config.ambiente.variavel.5.png) 

</div>

## Maven - 3ª Etapa - Váriaveis de ambiente - Path

2 - Ainda em "System variables", localize a variábel: Path;

3 - Clique no botão "Edit...";

4 - Clique no botão "New...";

5 - Insia o valor: %M2_HOME%\bin

6 - Confirme todas as telas no botão OK; 

<div align='center'>

<b>Figura - 7</b> 

![config.ambiente.variavel.7](doc/img/config.ambiente.variavel.7.png)

</div>

7 - Vamos então testar a instalação. Abra o CMD (linha de comando) e digite mvn -version.

# Selenium Drivers

Para utilizar a API do Selenium é necessário utilizar seus Drivers, mais comum, arquivos executaveis. Para cada tipo de navegador existe um driver especifico, a versão do navegador instalado na sua máquina influencia na versão do driver que vamos utilizar.

## Selenium - 1ª Etapa - Instalando o Driver

1 - Fazer download do driver para o navegador Google Chrome: [Clique aqui para acessar o link.](https://sites.google.com/a/chromium.org/chromedriver/downloads) 
 
 * Escolha versão conforme seu navegador;
 * Clique no link no nome da versão para ser direcionado para tela de download (Figura - 8);
 * Faça download do arquivo ZIP conforme o system type do seu Windows (Figura - 9 e 10); (Windows 10 possui o system type 64 bit, mas caso exista somente o driver para win32 não tem problema);

<div align='center'> 

<b>Figura - 8</b> 

![config.ambiente.selenium.driver.1](doc/img/config.ambiente.selenium.driver.1.png)

</div>

<div align='center'> 

<b>Figura - 9</b> 

![config.ambiente.selenium.driver.0](doc/img/config.ambiente.selenium.driver.0.png)

</div>

<div align='center'> 

<b>Figura - 10</b> 

![config.ambiente.selenium.driver.2](doc/img/config.ambiente.selenium.driver.2.png)

</div>

2 - Na unidade C: do seu computador, crie a estrutura de pastas a seguir:
  * C:\selenium\drivers\chrome\\<b>systemType_versaoDriver</b>\
    Exemplo conforme Figura 10: 
  
<div align='center'>

<b>Figura - 11</b> 

![config.ambiente.selenium.driver.3](doc/img/config.ambiente.selenium.driver.3.png)

</div>  
  
3 - Descarregar o driver que foi baixado, dentro da estrutura de pastas criada no passo anterior;

<div align='center'>

<b>Figura - 12</b> 

![config.ambiente.selenium.driver.4](doc/img/config.ambiente.selenium.driver.4.png)

</div>    