package parte1.navegacao;

import static org.testng.Assert.fail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class CampoDeTreinamentoTest {

	@Test
	public void deveAcessarCampoDeTreinamento() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
	
		driver.get(url);		
		
		driver.manage().window().maximize();
		
		driver.close();
		
		fail("Teste ainda n�o est� completo!");
		
	}
	
}
