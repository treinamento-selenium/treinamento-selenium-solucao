package parte2.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {
	
	protected String driverPath;
	protected String url;
	
	protected WebDriver driver;

	@BeforeTest
	public void preCon() {
		driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";
		url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
		System.setProperty("webdriver.chrome.driver", driverPath);
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
	}
	
	@AfterTest
	public void posCon() throws IOException {
		this.takesScreenShot();
		driver.quit();
	}
	
	public void takesScreenShot() throws IOException {		
		
		TakesScreenshot takeScreenshot = (TakesScreenshot) this.driver;		
		File srcFile = takeScreenshot.getScreenshotAs(OutputType.FILE);	
		
		StringBuilder fileName = new StringBuilder();
		fileName.append(this.getClass().getSimpleName())
				.append("_screenshot_")
				.append(this.getData())
				.append(".jpg")
				;
		
		StringBuilder destFileStr = new StringBuilder();
		destFileStr.append(System.getProperty("user.dir") )
				   .append("/target/")
				   .append(fileName)
				   ;
	
		File destFile = new File(destFileStr.toString());		
		FileUtils.copyFile(srcFile, destFile);	
	}
	
	public String getData() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}
	
}
