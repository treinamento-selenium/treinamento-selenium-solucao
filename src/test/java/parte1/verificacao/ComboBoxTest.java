package parte1.verificacao;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class ComboBoxTest {

	@Test
	public void deveSelecionarComboBox() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";

		System.setProperty("webdriver.chrome.driver", driverPath);

		WebDriver driver = new ChromeDriver();

		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";

		driver.get(url);

		driver.manage().window().maximize();		

		WebElement escolaridadeElement =  driver.findElement(By.id("elementosForm:escolaridade"));
		
		Select escolaridadeComboBox = new Select(escolaridadeElement);
		
		escolaridadeComboBox.selectByVisibleText("Superior");
		
		String escolaridadeSelecionada = "";
		
		escolaridadeSelecionada = escolaridadeComboBox.getFirstSelectedOption().getText();
		
		System.out.println("escolaridadeSelecionada: " + escolaridadeSelecionada);
		
		assertEquals(escolaridadeSelecionada, "Superior");
		
		WebElement praticaEsportesElement = driver.findElement(By.id("elementosForm:esportes"));
		
		Select praticaEsportesMultiComboBox = new Select(praticaEsportesElement);
		
		praticaEsportesMultiComboBox.selectByVisibleText("Futebol");
		
		praticaEsportesMultiComboBox.selectByVisibleText("Karate");	
		
		List<WebElement> elementList = praticaEsportesMultiComboBox.getAllSelectedOptions();	
		
		int qtdOpcoesSelecionadas = 0;
		
		qtdOpcoesSelecionadas = elementList.size();
		
		assertEquals(qtdOpcoesSelecionadas, 2);
		
		String esporteSelecionado = ""; 
		
		esporteSelecionado = elementList.get(0).getText();
		
		assertEquals(esporteSelecionado, "Futebol");
		
		esporteSelecionado = elementList.get(1).getText();
		
		assertEquals(esporteSelecionado, "Karate");
		
		for (WebElement element : elementList) {			
			
			esporteSelecionado = element.getText();
			
			System.out.println("Esportes Selecionados: " + esporteSelecionado);
			
		}			
			
		driver.close();
		
	}
	
}
