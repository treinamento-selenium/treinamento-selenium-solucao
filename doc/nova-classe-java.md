## Como criar uma nova classe Java

Neste treinamento vamos abordar a criação de testes desde o modelo mais básico até o modelo ideal previsto para o contexto Selenium, utilizando Design Pattern `PageObject`.

Como exemplo, vamos realizar a criação de uma classe Java no pacote `parte1.navegacao` que está dentro do pacote`src/test/java`. 

Conforme vimos no doc: [Como criar um novo pacote Java](doc/novo-pacote-java.md)

1 - Clique com o botão direito do mouse em cima do pacote `parte1.navegacao`.

2 - Clique em "New > Class". 

<div align='center'>

![config.eclipse.projeto.mavem.13](doc/img/config.eclipse.projeto.mavem.13.png)

</div> 

3 - No campo "Name" digite exatamente "NavegadoresWebTest" e depois clique em "Finish".

<div align='center'>

![config.eclipse.projeto.mavem.14](doc/img/config.eclipse.projeto.mavem.14.png)

</div> 

4 - Nova Classe criada com sucesso.

<div align='center'>

![config.eclipse.projeto.mavem.15](doc/img/config.eclipse.projeto.mavem.15.png)

</div> 

<b>Observações</b>:

* Nome de classe segue o padrão `CamelCase`, significa que a letra inicial do nome da classe SEMPRE deve ser MAIÚSCULA e o restante em letras minúsculas. Caso seja composto por mais de uma palavra, as palavras são concatenadas e seguem a mesma lógica. 

Fonte: https://pt.wikipedia.org/wiki/CamelCase

