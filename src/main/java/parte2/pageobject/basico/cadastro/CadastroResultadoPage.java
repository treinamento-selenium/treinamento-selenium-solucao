package parte2.pageobject.basico.cadastro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CadastroResultadoPage {
	
	private WebDriver driver;
	
	public CadastroResultadoPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public String statusCadastro() {
		String statusCadastro = "";
		statusCadastro = driver.findElement(By.xpath("//*[@id='resultado']/span")).getText();
		System.out.println("Status do Cadastro: " + statusCadastro);
		return statusCadastro;
	}
	
	public String nome() {
		String nome = "";
		nome = driver.findElement(By.xpath("//*[@id='descNome']/span")).getText();
		System.out.println("Nome: " + nome);
		return nome;
	}
	
	public String sobreNome() {
		String sobreNome = "";
		sobreNome = driver.findElement(By.xpath("//*[@id='descSobrenome']/span")).getText();
		System.out.println("Sobrenome: " + sobreNome);
		return sobreNome;
	}
	
	public String sexo() {
		String sexo = "";
		sexo = driver.findElement(By.xpath("//*[@id='descSexo']/span")).getText();
		System.out.println("Sexo: " + sexo);
		return sexo;	
	}
	
	public String comidaFavorita() {
		String comidaFavorita = "";
		comidaFavorita = driver.findElement(By.xpath("//*[@id='descComida']/span")).getText();
		System.out.println("Comida Favorita: " + comidaFavorita);
		return comidaFavorita;	
	}
	
	public String escolaridade() {
		String escolaridade = "";
		escolaridade = driver.findElement(By.xpath("//*[@id='descEscolaridade']/span")).getText();
		System.out.println("Escolaridade: " + escolaridade); 
		return escolaridade;	
	}
	
	public String esporte() {
		String esporte = "";
		esporte = driver.findElement(By.xpath("//*[@id='descEsportes']/span")).getText();
		System.out.println("Esporte: " + esporte);
		return esporte;	
	}
	
	public String sugestoes() {
		String sugestoes = "";
		sugestoes = driver.findElement(By.xpath("//*[@id='descSugestoes']/span")).getText();
		System.out.println("Sugest�es: " + sugestoes);
		return sugestoes;	
	}
	
}
