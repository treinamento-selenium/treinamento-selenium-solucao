package parte2.pageobject.basico.cadastro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class CadastroPage {
	
	private WebDriver driver;

	public CadastroPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public CadastroPage preencherCampoNome(String nome) {
		driver.findElement(By.id("elementosForm:nome")).sendKeys(nome);
		return this;
	}
	
	public CadastroPage preencherCampoSobreNome(String sobreNome) {
		driver.findElement(By.id("elementosForm:sobrenome")).sendKeys(sobreNome);
		return this;
	}
	
	public CadastroPage clicarNaOpcaoMasculinoNoCampoSexo() {
		driver.findElement(By.id("elementosForm:sexo:0")).click();
		return this;
	}
	
	public CadastroPage clicarNaOpcaoCarneNoCampoQualSuaComidaFavorita() {
		driver.findElement(By.id("elementosForm:comidaFavorita:0")).click();
		return this;
	}
	
	public CadastroPage selecionarOpcaoNoCampoEscolaridade(String escolaridade) {
		new Select(driver.findElement(By.id("elementosForm:escolaridade"))).selectByVisibleText(escolaridade);
		return this;
	}
	
	public CadastroPage seleconarOpcaoNoCampoPraticaEsportes(String esporte) {
		new Select(driver.findElement(By.id("elementosForm:esportes"))).selectByVisibleText(esporte);
		return this;
	}
	
	public CadastroPage preencherCampoSugestoes(String sugestoes) {
		driver.findElement(By.id("elementosForm:sugestoes")).sendKeys(sugestoes);
		return this;
	}
	
	public CadastroPage clicarNoBotaoCadastrar() {
		driver.findElement(By.id("elementosForm:cadastrar")).click();
		return this;
	}
	
}
