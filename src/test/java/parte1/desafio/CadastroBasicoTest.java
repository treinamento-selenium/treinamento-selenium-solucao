package parte1.desafio;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class CadastroBasicoTest {

	@Test
	public void deveCadastrarDadosBasicos() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
	
		driver.get(url);		
		
		driver.manage().window().maximize();
		
		String nome = "Roger";
		String sobreNome = "Azevedo";
		String sexo = "Masculino";
		String comidaFavorita = "Carne";
		String escolaridade = "Superior";
		String esporte = "O que eh esporte?";
		String sugestoes = "Usar um novo template\nNo pr�ximo encontro";		
		
		driver.findElement(By.id("elementosForm:nome")).sendKeys(nome);
		driver.findElement(By.id("elementosForm:sobrenome")).sendKeys(sobreNome);
		driver.findElement(By.id("elementosForm:sexo:0")).click();
		driver.findElement(By.id("elementosForm:comidaFavorita:0")).click();
		new Select(driver.findElement(By.id("elementosForm:escolaridade"))).selectByVisibleText(escolaridade);
		new Select(driver.findElement(By.id("elementosForm:esportes"))).selectByVisibleText(esporte);
		driver.findElement(By.id("elementosForm:sugestoes")).sendKeys(sugestoes);
		driver.findElement(By.id("elementosForm:cadastrar")).click();
		
		String msgSucessoCadastro = driver.findElement(By.xpath("//*[@id='resultado']/span")).getText();
		assertEquals(msgSucessoCadastro, "Cadastrado!");
		System.out.println("msgSucessoCadastro: " + msgSucessoCadastro);		
		
		String nomeResultado = driver.findElement(By.xpath("//*[@id='descNome']/span")).getText();
		assertEquals(nomeResultado, nome);
		System.out.println("nomeResultado: " + nomeResultado);
		
		String sobreNomeResultado = driver.findElement(By.xpath("//*[@id='descSobrenome']/span")).getText();
		assertEquals(sobreNomeResultado, sobreNome);
		System.out.println("sobreNomeResultado: " + sobreNomeResultado);
		
		String sexoResultado = driver.findElement(By.xpath("//*[@id='descSexo']/span")).getText();
		assertEquals(sexoResultado, sexo);
		System.out.println("sexoResultado: " + sexoResultado);
		
		String comidaFavoritaResultado = driver.findElement(By.xpath("//*[@id='descComida']/span")).getText();
		assertEquals(comidaFavoritaResultado, comidaFavorita);
		System.out.println("comidaFavoritaResultado: " + comidaFavoritaResultado);
		
		String escolaridadeResultado = driver.findElement(By.xpath("//*[@id='descEscolaridade']/span")).getText();
		assertEquals(escolaridadeResultado, escolaridade.toLowerCase());		
		System.out.println("escolaridadeResultado: " + escolaridadeResultado);
		
		String esporteResultado = driver.findElement(By.xpath("//*[@id='descEsportes']/span")).getText();
		assertEquals(esporteResultado, esporte);
		System.out.println("esporteResultado: " + esporteResultado);
		
		String sugestoesResultado = driver.findElement(By.xpath("//*[@id='descSugestoes']/span")).getText();
		assertEquals(sugestoesResultado, sugestoes.replace("\n", " "));
		System.out.println("sugestoesResultado: " + sugestoesResultado);		
		
		driver.close();
		
	}
	
}
