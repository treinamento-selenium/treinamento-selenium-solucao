package parte1.interacao;

import static org.testng.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TextFieldTextAreaTest {

	@Test
	public void devePreencherTextFieldTextArea() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
	
		driver.get(url);			
		
		driver.manage().window().maximize();
		
		driver.findElement(By.id("elementosForm:nome")).sendKeys("Roger");			
		
		driver.findElement(By.id("elementosForm:sugestoes")).sendKeys("Descreva uma sugest�o");	
		
		driver.findElement(By.id("elementosForm:sugestoes")).sendKeys("Descreva uma sugest�o 1\nDescreva uma sugest�o 2");
		
		driver.close();
		
		fail("Teste ainda n�o est� completo!");
		
	}
	
}
