package parte1.verificacao;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ButtonTest {

	@Test
	public void deveClicarButton() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";

		System.setProperty("webdriver.chrome.driver", driverPath);

		WebDriver driver = new ChromeDriver();

		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";

		driver.get(url);

		driver.manage().window().maximize();
		
		driver.findElement(By.id("buttonSimple")).click();
		
		String textoBotao = "";
		
		textoBotao = driver.findElement(By.id("buttonSimple")).getAttribute("value");
		
		System.out.println("textoBotao: " + textoBotao);
		
		assertEquals(textoBotao, "Obrigado!");
		
		driver.close();
		
	}
	
}
