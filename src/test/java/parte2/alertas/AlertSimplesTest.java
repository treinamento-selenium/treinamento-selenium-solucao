package parte2.alertas;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class AlertSimplesTest {

	@Test
	public void deveClicarNoAlertSimples() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
	
		driver.get(url);		
		
		driver.manage().window().maximize();		
		
		driver.findElement(By.id("alert")).click();
		
		String mensagemNoAlerta = "";
		
		Alert alert = driver.switchTo().alert();
		
		mensagemNoAlerta = alert.getText();
		
		System.out.println("mensagemNoAlerta: " + mensagemNoAlerta);
		
		alert.accept();		
			
		driver.close();
		
		assertEquals(mensagemNoAlerta, "Alert Simples");
		
	}
	
}
