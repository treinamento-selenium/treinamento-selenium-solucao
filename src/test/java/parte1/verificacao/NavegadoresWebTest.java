package parte1.verificacao;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class NavegadoresWebTest {

	@Test
	public void deveNavegarNoGoogleChrome() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "https://www.google.com";
	
		System.out.println("url:" + url);
		
		driver.get(url);
		
		driver.manage().window().maximize();
		
		String currentUrl = "";
		
		currentUrl = driver.getCurrentUrl();
		
		System.out.println("currentUrl: " + currentUrl);
		
		boolean contain = false;
		
		contain = currentUrl.contains(url);
		
		System.out.println("contain: " + contain);
		
		assertEquals(contain, true);
		
		driver.close();
		
	}
	
}
