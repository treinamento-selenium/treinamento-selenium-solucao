package parte2.esperas;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import parte2.util.BaseTest;

public class EsperaExplicitaTest extends BaseTest {

	@Test
	public void deveEsperarTempoExplicito() {
				
		driver.findElement(By.id("buttonDelay")).click();		
				
		By byTextField = By.id("novoCampo");		
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(byTextField));
		
		driver.findElement(byTextField).sendKeys("Deu certo?");
		
		String textoDoCampo = "";
		
		textoDoCampo = driver.findElement(byTextField).getAttribute("value");
		
		assertEquals(textoDoCampo, "Deu certo?");
		
	}
	
}
