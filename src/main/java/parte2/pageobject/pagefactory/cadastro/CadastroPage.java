package parte2.pageobject.pagefactory.cadastro;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CadastroPage {
	
	@FindBy (id = "elementosForm:nome")
	private WebElement nomeTextField;	
	
	@FindBy (id = "elementosForm:sobrenome")
	private WebElement sobreNomeTextField;
	
	@FindBy (id = "elementosForm:sexo:0")
	private WebElement sexoMasculinoRadioButton;
	
	@FindBy (id = "elementosForm:comidaFavorita:0")
	private WebElement comidaFavoritaCarneCheckBox;
	
	@FindBy (id = "elementosForm:escolaridade")
	private WebElement escolaridadeComboBox;
	
	@FindBy (id = "elementosForm:esportes")
	private WebElement esportesComboBox;
	
	@FindBy (id = "elementosForm:sugestoes")
	private WebElement sugestoesTextArea;
	
	@FindBy (id = "elementosForm:cadastrar")
	private WebElement cadastrarButton;
	
	public CadastroPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public CadastroPage limparCampoNome() {
		nomeTextField.clear();
		return this;
	}
	
	public CadastroPage preencherCampoNome(String nome) {
		nomeTextField.sendKeys(nome);
		return this;
	}
	
	public CadastroPage limparCampoSobreNome() {
		sobreNomeTextField.clear();
		return this;
	}
	
	public CadastroPage preencherCampoSobreNome(String sobreNome) {
		sobreNomeTextField.sendKeys(sobreNome);
		return this;
	}
	
	public CadastroPage clicarNaOpcaoMasculinoNoCampoSexo() {
		sexoMasculinoRadioButton.click();
		return this;
	}
	
	public CadastroPage clicarNaOpcaoCarneNoCampoQualSuaComidaFavorita() {
		comidaFavoritaCarneCheckBox.click();
		return this;
	}
	
	public CadastroPage selecionarOpcaoNoCampoEscolaridade(String escolaridade) {
		new Select(escolaridadeComboBox).selectByVisibleText(escolaridade);
		return this;
	}
	
	public CadastroPage seleconarOpcaoNoCampoPraticaEsportes(String esporte) {
		new Select(esportesComboBox).selectByVisibleText(esporte);
		return this;
	}
	
	public CadastroPage limparCampoSugestoes() {
		sugestoesTextArea.clear();
		return this;
	}
	
	public CadastroPage preencherCampoSugestoes(String sugestoes) {
		sugestoesTextArea.sendKeys(sugestoes);
		return this;
	}
	
	public CadastroPage clicarNoBotaoCadastrar() {
		cadastrarButton.click();
		return this;
	}
	
}
