## Como criar um novo pacote Java

Neste treinamento vamos abordar a criação de testes desde o modelo mais básico até o modelo ideal previsto para o contexto Selenium, utilizando Design Pattern `PageObject`.

Como exemplo, vamos realizar a criação de um pacote Java em `src/test/java`.

1 - Clique com o botão direito do mouse em cima da pasta `src/test/java`.

2 - Clique em "New > Package". 

<div align='center'>

![config.eclipse.projeto.mavem.11](doc/img/config.eclipse.projeto.mavem.11.png)

</div> 

3 - No campo "Name" digite exatamente "parte1.navegacao" e depois clique em "Finish".

<div align='center'>

![config.eclipse.projeto.mavem.12](doc/img/config.eclipse.projeto.mavem.12.png)

</div> 

<b>Observações</b>:

* Nome de pacote SEMPRE deve ser em letras minúsculas.
* Por "trás dos panos" o Java cria uma pasta com o nome do pacote.
* Caso o nome do pacote criado seja composto e separado por "." (ponto), o Java irá criar uma pasta dentro da outra. 

Por exemplo: `parte1.navegacao`

Será criado a pasta `parte1` e dentro dessa pasta, a pasta `navegacao`.
