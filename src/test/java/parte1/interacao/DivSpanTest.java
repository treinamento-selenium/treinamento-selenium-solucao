package parte1.interacao;

import static org.testng.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DivSpanTest {

	@Test
	public void deveCapturarTextoDivSpan() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
	
		driver.get(url);		
		
		driver.manage().window().maximize();
		
		String tituloPaginaEmTag = "";
		
		tituloPaginaEmTag = driver.findElement(By.tagName("h3")).getText();
		
		System.out.println("tituloPaginaEmTag: " + tituloPaginaEmTag);
		
		String textoDiv = "";
		
		textoDiv = driver.findElement(By.tagName("div")).getText();
		
		System.out.println("textoDivGetText: " + textoDiv);
		
		textoDiv = driver.findElement(By.tagName("div")).getAttribute("value");
		
		System.out.println("textoDivGetAttribute: " + textoDiv);
		
		textoDiv = driver.findElement(By.tagName("div")).getTagName();
		
		System.out.println("textoDivGetTagName: " + textoDiv);
		
		String textoCuidadoOndeClica = "";
		
		textoCuidadoOndeClica = driver.findElement(By.tagName("span")).getText();
		
		System.out.println("textoCuidadoOndeClica: " + textoCuidadoOndeClica);		
		
		textoCuidadoOndeClica = driver.findElement(By.className("facilAchar")).getText();
		
		System.out.println("textoCuidadoOndeClica: " + textoCuidadoOndeClica);
		
		driver.close();
		
		fail("Teste ainda n�o est� completo!");
		
	}
	
}
