package parte2.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DSLSelenium {
	
	private WebDriver driver;
	
	public DSLSelenium(WebDriver driver) {
		this.driver = driver;
	}
	
	public DSLSelenium clicarCampo(final By findBy) {
		driver.findElement(findBy).click();
		return this;
	}
	
	public DSLSelenium preencherCampo(final By findBy, String texto) {
		driver.findElement(findBy).sendKeys(texto);
		return this;
	}
	
	public DSLSelenium selecionarOpcaoVisivelCampo(final By findBy, String opcao) {
		WebElement element = driver.findElement(findBy);
		Select select = new Select(element);
		select.selectByVisibleText(opcao);
		return this;
	}
	
	public String capturarValorDoElemento(final By findBy) {
		String valor = "";
		valor = driver.findElement(findBy).getAttribute("Value");
		System.err.println(findBy + ": Valor do elemento: " + valor);
		return valor;
	}
	
	public String capturarTextoDoElemento(final By findBy) {
		String texto = "";
		texto = driver.findElement(findBy).getText();
		System.out.println(findBy + ": Texto do elemento: " + texto);
		return texto;
	}
	
}
