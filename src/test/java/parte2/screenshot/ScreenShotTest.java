package parte2.screenshot;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.Test;

import parte2.util.BaseTest;

public class ScreenShotTest extends BaseTest {

	@Test
	public void deveCapturarImagemDaTela() throws IOException {		
				
		TakesScreenshot takeScreenshot = (TakesScreenshot) this.driver;
		
		File srcFile = takeScreenshot.getScreenshotAs(OutputType.FILE);
		
		String destFileStr = "screenshot.jpg";
		
		File destFile = new File(destFileStr);
		
		FileUtils.copyFile(srcFile, destFile);		
		
		assertEquals(destFile.exists(), true);
		
		System.out.println(destFile.getAbsolutePath());
		
	}
	
}
