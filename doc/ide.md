# Doc - IDE (Instalação Eclipse)

Como ferramente de desenvolvimento vamos utilizar o Eclipse, para facilitar e focar na didática.

<b>Considerações:</b>

- Sistema Operacional: Windows 7 ou superior.
- JDK: Versão igual ou superior a 1.8.
- Eclipse: Versão mais recente
- Durante a execução de uma Etapa, verifique se no seu computador já existe a configuração especificada na etapa.

## Eclipse

Eclipse é um IDE para desenvolvimento Java, porém suporta várias outras linguagens a partir de plugins como C/C++, PHP, ColdFusion, Python, Scala e plataforma Android. Ele foi feito em Java e segue o modelo open source de desenvolvimento de software. Wikipédia

## Eclipse - 1ª Etapa - Instalação

1 - Faza o download e instalação do Eclipse: [Clique aqui para acessar a página de download.](https://www.eclipse.org/downloads/)