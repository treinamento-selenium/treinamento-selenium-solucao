package parte2.esperas;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import parte2.util.BaseTest;

public class EsperaImplicitaTest extends BaseTest {

	@Test
	public void deveEsperarTempoImplicito() {
				
		driver.findElement(By.id("buttonDelay")).click();		
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		driver.findElement(By.id("novoCampo")).sendKeys("Deu certo?");		
		
		String textoDoCampo = "";
		
		textoDoCampo = driver.findElement(By.id("novoCampo")).getAttribute("value");
		
		assertEquals(textoDoCampo, "Deu certo?");
		
	}
	
}
