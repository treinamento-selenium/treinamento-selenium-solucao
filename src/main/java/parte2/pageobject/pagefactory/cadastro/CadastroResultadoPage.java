package parte2.pageobject.pagefactory.cadastro;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CadastroResultadoPage {
	
	@FindBy (xpath = "//*[@id='resultado']/span")
	private WebElement statusCadastroLabel;
	
	@FindBy (xpath = "//*[@id='descNome']/span")
	private WebElement nomeLabel;
	
	@FindBy (xpath = "//*[@id='descSobrenome']/span")
	private WebElement sobreNomeLabel;
	
	@FindBy (xpath = "//*[@id='descSexo']/span")
	private WebElement sexoLabel;
	
	@FindBy (xpath = "//*[@id='descComida']/span")
	private WebElement comidaLabel;
	
	@FindBy (xpath = "//*[@id='descEscolaridade']/span")
	private WebElement escolaridadeLabel;
	
	@FindBy (xpath = "//*[@id='descEsportes']/span")
	private WebElement esperteLabel;
	
	@FindBy (xpath = "//*[@id='descSugestoes']/span")
	private WebElement sugestoesLabel;
	
	public CadastroResultadoPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public String statusCadastro() {
		String statusCadastro = "";
		statusCadastro = statusCadastroLabel.getText();
		System.out.println("Status do Cadastro: " + statusCadastro);
		return statusCadastro;
	}
	
	public String nome() {
		String nome = "";
		nome = nomeLabel.getText();
		System.out.println("Nome: " + nome);
		return nome;
	}
	
	public String sobreNome() {
		String sobreNome = "";
		sobreNome = sobreNomeLabel.getText();
		System.out.println("Sobrenome: " + sobreNome);
		return sobreNome;
	}
	
	public String sexo() {
		String sexo = "";
		sexo = sexoLabel.getText();
		System.out.println("Sexo: " + sexo);
		return sexo;	
	}
	
	public String comidaFavorita() {
		String comidaFavorita = "";
		comidaFavorita = comidaLabel.getText();
		System.out.println("Comida Favorita: " + comidaFavorita);
		return comidaFavorita;	
	}
	
	public String escolaridade() {
		String escolaridade = "";
		escolaridade = escolaridadeLabel.getText();
		System.out.println("Escolaridade: " + escolaridade); 
		return escolaridade;	
	}
	
	public String esporte() {
		String esporte = "";
		esporte = esperteLabel.getText();
		System.out.println("Esporte: " + esporte);
		return esporte;	
	}
	
	public String sugestoes() {
		String sugestoes = "";
		sugestoes = sugestoesLabel.getText();
		System.out.println("Sugest�es: " + sugestoes);
		return sugestoes;	
	}
	
}
