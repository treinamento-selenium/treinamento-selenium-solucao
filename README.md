## Treinamento Selenium WebDriver

Projeto de estudos para automação de testes funcionais, desenvolvido para possibilitar a capacitação de uso dos recursos básicos e essenciais do Selenium. 

Facilitando a criação de casos de testes automatizados no dia-a-dia.

Não faz parte do escopo de estudos entrar em detalhes técnicos do funcionamento interno da API do Selenium. O foco é o uso básico e prático.

## Conhecimentos necessários

* Levantamento e Especificação de cenários de testes;
* Lógica de Programação;
* Programação Orientada a Objetos (OOP);
* Linguagem de programação Java;
* IDE Eclipse;

## Selenium

Selenium é um conjunto de ferramentas especificamente para automatizar navegadores da web.

O Selenium WebDriver faz chamadas diretamente ao navegador utilizando o suporte à automação nativo de cada navegador. Assim os testes escritos com o WebDriver são bastante realistas, pois chama diretamente o navegador. Além disso, o Selenium Webdriver suporta praticamente todos os navegadores web existentes: Google Chrome, Firefox, Internet Explorer, Safari, Opera, etc.

* Fonte: https://www.seleniumhq.org/about/
* Doc: https://www.seleniumhq.org/docs/

## Teste Funcional

O teste funcional é a investigação do software a fim de fornecer informações sobre sua qualidade em relação ao contexto em que ele deve operar, se relaciona com o conceito de verificação e validação. 

Isso inclui o processo de utilizar o produto para encontrar seus defeitos.

* Fonte: https://pt.wikipedia.org/wiki/Teste_de_software

## Verificação x Validação

<b>Verificação</b>

* Estamos fazendo corretamente o software? 
* Aderência aos requisitos especificados .

<b>Validação</b>

* Estamos fazendo o software correto?
* Aderência aos requisitos desejados do usuário.

<b>Exemplo:</b>

VALIDAMOS com o cliente se o sistema que ele está solicitando deve ser na cor AZUL.

O cliente diz que sim, é isso que ele deseja.
Então, especificamos nos requisitos que o sistema DEVE ser na cor AZUL.

Então, desenvolvemos o sistema na cor AZUL.

Então nós [QA ou TESTER] VERIFICAMOS se o sistema foi desenvolvido na cor AZUL.

## O que é automação de testes funcionais?

É o uso de software para controlar a execução do teste de software, a comparação dos resultados esperados com os resultados reais, a configuração das pré-condições de teste e outras funções de controle e relatório de teste. De forma geral, a automação de teste pode iniciar a partir de um processo manual de teste já estabelecido e formalizado.

Fonte: https://pt.wikipedia.org/wiki/Automação_de_teste

## Porquê fazer automação de testes?

* Ganhar tempo na execução de testes regressivos.
* Validar que o que funcionava na entrega anterior, continua funcionando para a próxima entrega.
* Gerar feedback rápido para o time, com alta velocidade de execução, viabilizando a correção de bugs em funcionalidades que foram impactadas de forma mais efetiva.
* A automação de testes libera a equipe de testes para a elaboração de testes mais complexos e que exijam decisões humanas, como testes de usabilidade, segurança e exploratórios.

## Como vamos aplicar verificação no contexto de testes automatizados?

<b>Comparar resultado [atual] com [esperado].</b>

<b>Verdadeiro ou Falso.</b>

* Visível ou não visível
* Selecionado ou não selecionado
* Contém ou não contém

<b>Asserção</b>

Em computação, asserção (em inglês: assertion ) é um predicado que é inserido no programa para verificar uma condição que o desenvolvedor supõe que seja verdadeira em determinado ponto.

* Fonte: https://pt.wikipedia.org/wiki/Asserção


## Página/Sistema Web

Também conhecida no inglês como webpage, é uma "página" na internet, geralmente em formato HTML e com ligações de hipertexto que permitem a navegação de uma página para outra. Usam com frequência recursos gráficos para fins de ilustração, que também podem ter ligações clicáveis. É apresentada com o recurso de um navegador (browser)...

Fonte: https://pt.wikipedia.org/wiki/Página_web

## Como começar?

* <b>Configurações</b>
    1. [Ambiente (Java, Maven, Variáveis de ambiente)](doc/ambiente.md)
    2. [IDE (Intalação Eclipse)](doc/ide.md)
    3. [Instalar Plugin do TestNG no Eclipse](https://www.toolsqa.com/selenium-webdriver/install-testng/)
    4. [Criar um novo projeto maven](doc/novo-projeto-maven.md)
    5. [Como criar um novo pacote Java](doc/novo-pacote-java.md)
    6. [Como criar uma nova classe Java](doc/nova-classe-java.md)
    7. [Como criar um método de teste](doc/metodo-de-teste.md)

## Parte 1    

* <b>Navegação</b>;
	1. Abrir um navegador
	2. Acessar uma página web
	3. Ler o título de uma página web
	4. Ler uma URL
	5. Pegar um texto da página web

* <b>Interação</b>
	* Tipos de elementos básicos
	* Ações aplicáveis nos elementos
		* Clicar
		* Preencher
		* Selecionar


* <b>Verificação de resultado</b>
	* Asserções	
	
## Parte 2	

* Alertas
* Frames
* Janelas
* WindowHandler
* Esperas
* Captura de tela
* PageObjects

[Clique aqui para acesser apresentação e material do treinamento (PPT)](https://drive.google.com/open?id=1-MQRAFMeBMZEYRqApUUFs0v_SPmoBtk0)

## Tecnologias e referências utilizadas neste projeto

* [TestNG Doc: ](https://testng.org/doc/)
* [TestNG Install Plugin Eclipse: ](https://www.toolsqa.com/selenium-webdriver/install-testng/)
* [Google Chrome Navegador: ](https://www.google.com/intl/pt-BR_ALL/chrome/)
* [HTML: ](http://tableless.github.io/iniciantes/manual/html/estruturabasica.html)
* [Java doc Thread: ](https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.html)
* [Selenium Wait Implicit vs Explicit: ](http://stackoverflow.com/questions/15164742/combining-implicit-wait-and-explicit-wait-together-results-in-unexpected-wait-ti/15174978#15174978)
* [Java diferença entre modificadores de acesso: ](https://pt.stackoverflow.com/questions/23/qual-%C3%A9-a-diferen%C3%A7a-entre-modificadores-public-default-protected-e-private)
* [TestNG Notações: ](https://testng.org/doc/documentation-main.html#annotations)
