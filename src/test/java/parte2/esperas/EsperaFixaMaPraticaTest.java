package parte2.esperas;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import parte2.util.BaseTest;

public class EsperaFixaMaPraticaTest extends BaseTest {

	@Test
	public void deveEsperarTempoFixo() throws InterruptedException {
		
		driver.findElement(By.id("buttonDelay")).click();
		
		Thread.sleep(3000);
		
		driver.findElement(By.id("novoCampo")).sendKeys("Deu certo?");		
		
		String textoDoCampo = "";
		
		textoDoCampo = driver.findElement(By.id("novoCampo")).getAttribute("value");
		
		assertEquals(textoDoCampo, "Deu certo?");
		
	}
	
}
