# Doc - Criar um novo projeto maven

Para facilitar e focar na didática, vamos considerar os itens abaixo como objeto de estudo e tutorial de configuração.

<b>Considerações:</b>

- Sistema Operacional: Windows10 - Versão em inglês.
- Antes de iniciar uma Etapa, verifique se já possui a configuração. 

<b>Tópicos</b>

  * [1ª Etapa - Criar workspace](#criar-workspace)
  * [2ª Etapa - Criar um novo projeto](#criar-um-novo-projeto)
  * [3ª Etapa - Configurar dependências do projeto](#configurar-dependencias-do-projeto)
    
## 1ª Etapa - Criar workspace

1 - No seu computar, dentro da sua pasta de usuário: C:\Users\\<b>{seu-usuario}</b>\

2 - Crie uma pasta chamada: "workspace-treinamento-selenium": Exemplo: C:\Users\rogera\workspace-treinamento-selenium

## 2ª Etapa - Criar um novo projeto

1 - Abra o seu Eclipse e selecione o workspace recém criado;

<div align='center'>

<b>Figura - 1</b> 

![config.eclipse.projeto.mavem.1](doc/img/config.eclipse.projeto.mavem.1.png)

</div>   

2 - O Eclipse irá iniciar na tela de boas vindas: "welcome"

<div align='center'>

<b>Figura - 2</b>

![config.eclipse.projeto.mavem.2](doc/img/config.eclipse.projeto.mavem.2.png)

</div>   

3 - Vamos acessar o menu "File > New > Other";

<div align='center'>

<b>Figura - 3</b>

![config.eclipse.projeto.mavem.3](doc/img/config.eclipse.projeto.mavem.3.png)

</div>   

4 - Digite "Maven" no campo "Wizards";

5 - Selecione a opção "Maven Project" e clique no botão "Next";

<div align='center'>

<b>Figura - 4</b>

![config.eclipse.projeto.mavem.4](doc/img/config.eclipse.projeto.mavem.4.png)

</div>  

6 - Selecione o checkbox na opção "Create a simple project (skip archetype selection)" e clique no botão "Next";

<div align='center'>

<b>Figura - 5</b>

![config.eclipse.projeto.mavem.5](doc/img/config.eclipse.projeto.mavem.5.png)

</div> 

7 - Informe nos campos abaixo os seguintes valores e em seguida clique no botão "Finish":

   * "Group Id"; "treinamento-selenium" 
   * "Artifact Id"; "treinamento-selenium"
   
<div align='center'>

<b>Figura - 6</b> 

![config.eclipse.projeto.mavem.6](doc/img/config.eclipse.projeto.mavem.6.png)

</div>    

8 - Podemos fechar a aba "Welcome" e expandir a aba "Package Explore";

9 - Expanda o projeto;

<div align='center'>

<b>Figura - 7</b> 

![config.eclipse.projeto.mavem.7](doc/img/config.eclipse.projeto.mavem.7.png)

</div>   

<b>Estrutura padrão de um projeto Maven</b>

A estrutura padrão do projeto inclui boas práticas (como separar as classes de teste das classes do sistema) e facilita aos novos desenvolvedores encontrar o que eles querem, já que todos os projetos seguirão uma estrutura semelhante.

Veja a seguir os principais diretórios utilizados:

* <b>src/main/java:</b> aqui fica o código-fonte do sistema ou biblioteca.
* <b>src/main/resources:</b> arquivos auxiliares do sistema, como .properties, XMLs e configurações.
* <b>src/main/webapp:</b> se for uma aplicação web, os arquivos JSP, HTML, JavaScript CSS vão aqui, incuindo o web.xml.
* <b>src/test/java:</b> as classes com seus testes unitários ficam aqui e são executadas automaticamente com JUnit e TestNG. Outros frameworks podem exigir configuração adicional.
* <b>src/test/resources:</b> arquivos auxiliares usados nos testes. Você pode ter properties e configurações alternativas, por exemplo.
* <b>pom.xml:</b> é o arquivo que concentra as informações do seu projeto.
* <b>target:</b> é o diretório onde fica tudo que é gerado, isto é, onde vão parar os arquivos compilados, JARs, WARs, JavaDoc, etc.

## 3ª Etapa - Configurar dependências do projeto

<b>Arquivo POM</b>

O arquivo pom (pom.xml) é a configuração principal do Maven e estará presente em todo projeto. Nele você declara a identificação do seu projeto (que após gerado será também um artefato Maven), as dependências, repositórios adicionais, etc.

Há um arquivo pom por projeto, mas ele pode herdar configurações de um parent pom, isto é, como se houvesse um projeto "pai".

1 - Abra o arquivo "pom.xml" do projeto no Eclipse;

2 - E clique na aba "pom.xml";

<div align='center'>

<b>Figura - 8</b> 

![config.eclipse.projeto.mavem.8](doc/img/config.eclipse.projeto.mavem.8.png)

</div> 

3 - Clique com o mouse no final da linha 5 e pressione "Enter" para inserir uma nova linha;

<div align='center'>

<b>Figura - 9</b> 

![config.eclipse.projeto.mavem.9](doc/img/config.eclipse.projeto.mavem.9.png)

</div> 

4 - Copie e cole o código abaixo para adicionar as dependências que vamos utilizar no projeto e depois salve as alterações;

```
<dependencies>
  	<dependency>
  		<groupId>org.testng</groupId>
  		<artifactId>testng</artifactId>
  		<version>6.14.2</version>
  	</dependency>
  	<dependency>
  		<groupId>org.seleniumhq.selenium</groupId>
  		<artifactId>selenium-java</artifactId>
  		<version>3.11.0</version>
  	</dependency>
  	<dependency>
  		<groupId>commons-io</groupId>
  		<artifactId>commons-io</artifactId>
  		<version>2.4</version>
  	</dependency>
  </dependencies>

```

<div align='center'>

<b>Figura - 10</b> 

![config.eclipse.projeto.mavem.10](doc/img/config.eclipse.projeto.mavem.10.png)

</div> 