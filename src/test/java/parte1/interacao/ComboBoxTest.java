package parte1.interacao;

import static org.testng.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class ComboBoxTest {

	@Test
	public void deveSelecionarComboBox() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";

		System.setProperty("webdriver.chrome.driver", driverPath);

		WebDriver driver = new ChromeDriver();

		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";

		driver.get(url);

		driver.manage().window().maximize();		

		WebElement escolaridadeElement =  driver.findElement(By.id("elementosForm:escolaridade"));
		
		Select escolaridadeComboBox = new Select(escolaridadeElement);
		
		escolaridadeComboBox.selectByVisibleText("Superior");
		
		WebElement praticaEsportesElement = driver.findElement(By.id("elementosForm:esportes"));
		
		Select praticaEsportesMultiComboBox = new Select(praticaEsportesElement);
		
		praticaEsportesMultiComboBox.selectByVisibleText("Futebol");
		
		praticaEsportesMultiComboBox.selectByVisibleText("Karate");
				
		driver.close();
		
		fail("Teste ainda n�o est� completo!");
		
	}
	
}
