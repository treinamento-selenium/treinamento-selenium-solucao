package parte2.dsl.selenium;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import parte2.pageobject.cadastro.CadastroDTO;
import parte2.util.BaseTest;
import parte2.util.DSLSelenium;

public class CadastroBasicoDSLSeleniumTest extends BaseTest {

	@Test
	public void deveCadastrarDadosBasicos() {
				
		CadastroDTO cadastroDTO = new CadastroDTO();
		
		cadastroDTO.setStatus("Cadastrado!");
		cadastroDTO.setNome("Roger");
		cadastroDTO.setSobreNome("de Azevedo");
		cadastroDTO.setSexo("Masculino");
		cadastroDTO.setComidaFavorita("Carne");
		cadastroDTO.setEscolaridade("Superior");
		cadastroDTO.setEsporte("Futebol");
		cadastroDTO.setSugestoes("Sugestao linha 1\nSugestao linha 2");
		
		System.out.println(cadastroDTO.toString());
		
		DSLSelenium dslSelenium = new DSLSelenium(driver);		
		
		dslSelenium.preencherCampo(By.id("elementosForm:nome"), cadastroDTO.getNome());
		dslSelenium.preencherCampo(By.id("elementosForm:sobrenome"), cadastroDTO.getSobreNome());
		dslSelenium.clicarCampo(By.id("elementosForm:sexo:0"));
		dslSelenium.clicarCampo(By.id("elementosForm:comidaFavorita:0"));
		dslSelenium.selecionarOpcaoVisivelCampo(By.id("elementosForm:escolaridade"), cadastroDTO.getEscolaridade());
		dslSelenium.selecionarOpcaoVisivelCampo(By.id("elementosForm:esportes"), cadastroDTO.getEsporte());
		dslSelenium.preencherCampo(By.id("elementosForm:sugestoes"), cadastroDTO.getSugestoes());
		dslSelenium.clicarCampo(By.id("elementosForm:cadastrar"));
		
		assertEquals(dslSelenium.capturarTextoDoElemento(By.xpath("//*[@id='resultado']/span")), cadastroDTO.getStatus());		
		assertEquals(dslSelenium.capturarTextoDoElemento(By.xpath("//*[@id='descNome']/span")), cadastroDTO.getNome());
		assertEquals(dslSelenium.capturarTextoDoElemento(By.xpath("//*[@id='descSobrenome']/span")), cadastroDTO.getSobreNome());
		assertEquals(dslSelenium.capturarTextoDoElemento(By.xpath("//*[@id='descSexo']/span")), cadastroDTO.getSexo());
		assertEquals(dslSelenium.capturarTextoDoElemento(By.xpath("//*[@id='descComida']/span")), cadastroDTO.getComidaFavorita());
		assertEquals(dslSelenium.capturarTextoDoElemento(By.xpath("//*[@id='descEscolaridade']/span")), cadastroDTO.getEscolaridade().toLowerCase());
		assertEquals(dslSelenium.capturarTextoDoElemento(By.xpath("//*[@id='descEsportes']/span")), cadastroDTO.getEsporte());
		assertEquals(dslSelenium.capturarTextoDoElemento(By.xpath("//*[@id='descSugestoes']/span")), cadastroDTO.getSugestoes().replace("\n", " "));
		
	}
	
}
