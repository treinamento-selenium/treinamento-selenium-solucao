package parte1.navegacao;

import static org.testng.Assert.fail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class CapturaDeTextoDaPaginaWebTest {

	@Test
	public void deveCapturarTextoComGoogleChrome() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();	
		
		String url = "http:\\www.google.com";
	
		driver.get(url);
		
		driver.manage().window().maximize();
		
		String tituloPagina = "";
		
		tituloPagina = driver.getTitle();		
		
		System.out.println("T�tulo da p�gina: " + tituloPagina);
		
		String urlAtual = "";
		
		urlAtual = driver.getCurrentUrl();
		
		System.out.println("URL Atual: " + urlAtual);
		
		String codigoFonte = "";
		
		codigoFonte = driver.getPageSource();
		
		System.out.println("codigoFonte: " + codigoFonte);		
		
		driver.close();
		
		fail("Teste ainda n�o est� completo!");
		
	}
	
}
