package parte1.verificacao;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class RadioButtonCheckBoxTest {

	@Test
	public void deveClicarRadioButtonCheckBox() {

		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";

		System.setProperty("webdriver.chrome.driver", driverPath);

		WebDriver driver = new ChromeDriver();

		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";

		driver.get(url);

		driver.manage().window().maximize();		

		driver.findElement(By.id("elementosForm:sexo:0")).click();
		
		boolean opcaoDeSexoFoiSelecionado = driver.findElement(By.id("elementosForm:sexo:0")).isSelected();
		
		System.out.println("opcaoDeSexoFoiSelecionado: " + opcaoDeSexoFoiSelecionado);
		
		assertEquals(opcaoDeSexoFoiSelecionado, true);
		
		driver.findElement(By.id("elementosForm:comidaFavorita:2")).click();
		
		boolean opcaoDeComidaFavoritaFoiSelecionado = driver.findElement(By.id("elementosForm:comidaFavorita:2")).isSelected();
		
		System.out.println("opcaoDeComidaFavoritaFoiSelecionada: " + opcaoDeComidaFavoritaFoiSelecionado);
		
		assertEquals(opcaoDeComidaFavoritaFoiSelecionado, true);
				
		driver.close();
		
	}

}
