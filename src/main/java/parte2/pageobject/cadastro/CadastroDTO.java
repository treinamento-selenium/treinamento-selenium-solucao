package parte2.pageobject.cadastro;

public class CadastroDTO {

	private String status;
	private String nome;
	private String sobreNome;
	private String sexo;
	private String comidaFavorita;
	private String escolaridade;
	private String esporte;
	private String sugestoes;
	
	public CadastroDTO() {}

	@Override
	public String toString() {
		return "CadastroDTO [status=" + status + ", nome=" + nome + ", sobreNome=" + sobreNome
				+ ", sexo=" + sexo + ", comidaFavorita=" + comidaFavorita + ", escolaridade=" + escolaridade
				+ ", esporte=" + esporte + ", sugestoes=" + sugestoes + "]";
	}

	public String getNome() {
		return nome;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobreNome() {
		return sobreNome;
	}

	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getComidaFavorita() {
		return comidaFavorita;
	}

	public void setComidaFavorita(String comidaFavorita) {
		this.comidaFavorita = comidaFavorita;
	}

	public String getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getEsporte() {
		return esporte;
	}

	public void setEsporte(String esporte) {
		this.esporte = esporte;
	}

	public String getSugestoes() {
		return sugestoes;
	}

	public void setSugestoes(String sugestoes) {
		this.sugestoes = sugestoes;
	}	
	
}
