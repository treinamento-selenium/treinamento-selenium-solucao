package parte2.pageobject.pagefactory.cadastro;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import parte2.pageobject.cadastro.CadastroDTO;
import parte2.util.BaseTest;


public class CadastroBasicoPageFactoryTest extends BaseTest {

	@Test
	public void deveCadastrarDadosBasicos() {
		
		CadastroDTO cadastroDTO = new CadastroDTO();
		
		cadastroDTO.setStatus("Cadastrado!");
		cadastroDTO.setNome("Roger");
		cadastroDTO.setSobreNome("de Azevedo");
		cadastroDTO.setSexo("Masculino");
		cadastroDTO.setComidaFavorita("Carne");
		cadastroDTO.setEscolaridade("Superior");
		cadastroDTO.setEsporte("Futebol");
		cadastroDTO.setSugestoes("Sugestao linha 1\nSugestao linha 2");
		
		System.out.println("Dados de entrada: \n" + cadastroDTO.toString());
				
		CadastroPage cadastroPage = new CadastroPage(driver);
		
		cadastroPage.limparCampoNome()
						.preencherCampoNome(cadastroDTO.getNome());
		
		cadastroPage.limparCampoSobreNome()
						.preencherCampoSobreNome(cadastroDTO.getSobreNome());
		
		cadastroPage.clicarNaOpcaoMasculinoNoCampoSexo()
						.clicarNaOpcaoCarneNoCampoQualSuaComidaFavorita()
							.selecionarOpcaoNoCampoEscolaridade(cadastroDTO.getEscolaridade())
								.seleconarOpcaoNoCampoPraticaEsportes(cadastroDTO.getEsporte());
		
		cadastroPage.limparCampoSugestoes()						
						.preencherCampoSugestoes(cadastroDTO.getSugestoes());
		
		cadastroPage.clicarNoBotaoCadastrar();
		
		CadastroResultadoPage cadastroResultadoPage = new CadastroResultadoPage(driver);
		
		assertEquals(cadastroResultadoPage.statusCadastro(), cadastroDTO.getStatus());		
		assertEquals(cadastroResultadoPage.nome(), cadastroDTO.getNome());		
		assertEquals(cadastroResultadoPage.sobreNome(), cadastroDTO.getSobreNome());
		assertEquals(cadastroResultadoPage.sexo(), cadastroDTO.getSexo());		
		assertEquals(cadastroResultadoPage.escolaridade(), cadastroDTO.getEscolaridade().toLowerCase());
		assertEquals(cadastroResultadoPage.comidaFavorita(), cadastroDTO.getComidaFavorita());
		assertEquals(cadastroResultadoPage.esporte(), cadastroDTO.getEsporte());
		assertEquals(cadastroResultadoPage.sugestoes(), cadastroDTO.getSugestoes().replace("\n", " "));
		
	}
	
}
