package parte1.navegacao;

import static org.testng.Assert.fail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class NavegadoresWebTest {

	@Test
	public void deveNavegarNoGoogleChrome() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
				
		driver.manage().window().maximize();
		
		String url = "http:\\www.google.com";
		
		driver.get(url);
		
		driver.close();
		
		fail("Teste ainda n�o est� completo!");
		
	}
	
}
