package parte1.verificacao;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class LinkTest {

	@Test
	public void deveClicarLink() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";

		System.setProperty("webdriver.chrome.driver", driverPath);

		WebDriver driver = new ChromeDriver();

		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";

		driver.get(url);

		driver.manage().window().maximize();
		
		driver.findElement(By.linkText("Voltar")).click();
		
		String mensagemResultadoCliqueLinkVoltar = "";
		
		mensagemResultadoCliqueLinkVoltar = driver.findElement(By.id("resultado")).getText();
		
		System.out.println("mensagemRespostaCliqueLinkVoltar: " + mensagemResultadoCliqueLinkVoltar);
		
		driver.close();
		
	}
	
}
