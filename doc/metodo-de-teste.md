## Como criar um método de teste

Neste treinamento vamos abordar a criação de testes desde o modelo mais básico até o modelo ideal previsto para o contexto Selenium, utilizando Design Pattern `PageObject`.

Como exemplo, vamos realizar a criação de um método de teste na classe Java `NavegadoresWebTest` que está  no pacote `parte1.navegacao` que está dentro do pacote`src/test/java`.

Conforme vimos no doc: [Como criar uma nova classe Java](doc/nova-classe-java.md)

1 - Na Classe `NavegadoresWebTest`, vamos implementar um novo método público e estático, sem retorno, chamado `deveNavegarNoGoogleChrome`.

<div align='center'>

![config.eclipse.projeto.mavem.16](doc/img/config.eclipse.projeto.mavem.16.png)

</div> 

2 - Como estamos usando o `TestNG` para executar os testes, precisamos adicionar a notação `@Test` no método de teste.

* É necessário que a notação fique exatamente em cima do método.
* É necessário realizar o import da notação para o Java entender que queremos usar esse recurso.

<div align='center'>

![config.eclipse.projeto.mavem.17](doc/img/config.eclipse.projeto.mavem.17.png)

</div> 

3 - Dentro do método `deveNavegarNoGoogleChrome`, vamos definir variável para localizar o driver do navegador.

<div align='center'>

![config.eclipse.projeto.mavem.18](doc/img/config.eclipse.projeto.mavem.18.png)

</div> 

4 - Vamos definir as propriedades do driver para o sistema operacional poder localiza-lo.

<div align='center'>

![config.eclipse.projeto.mavem.19](doc/img/config.eclipse.projeto.mavem.19.png)

</div> 

5 - Vamos definir instância do driver para o navegador ChromeDriver.

* É necessário realizar o import das classes `WebDriver` e `ChromeDriver` para o Java entender que queremos usar esses recursos.

<div align='center'>

![config.eclipse.projeto.mavem.20](doc/img/config.eclipse.projeto.mavem.20.png)

</div> 

6 - Vamos definir a variável de `url` para o site do google `http:\\www.google.com`

<div align='center'>

![config.eclipse.projeto.mavem.21](doc/img/config.eclipse.projeto.mavem.21.png)

</div> 

7 - Agora vamos dizer para o driver que queremos acessar essa `url` e maximizar o navegador.

<div align='center'>

![config.eclipse.projeto.mavem.22](doc/img/config.eclipse.projeto.mavem.22.png)

</div> 

8 - E vamos dizer para ele encerrar tudo e fechar o navegador.

<div align='center'>

![config.eclipse.projeto.mavem.23](doc/img/config.eclipse.projeto.mavem.23.png)

</div> 

9 - Por fim e como boa prática, como não estamos realizando nenhum teste, ou seja, irá gerar um falso resultado positivo. Para evitar isso vamos forçar falha.

* É necessário realizar o import das classe `Assert` para o Java entender que queremos usar esses recursos. 

<div align='center'>

![config.eclipse.projeto.mavem.24](doc/img/config.eclipse.projeto.mavem.24.png)

</div> 

<b>Observações</b>:

* Nome de método segue um padrão similar ao `CamelCase`, exceto que a letra inicial do nome SEMPRE deve ser minúscula. 

Fonte: https://pt.wikipedia.org/wiki/CamelCase

