package parte2.janelas;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class JanelasPopUpSemIdentificadorTest {

	@Test
	public void deveInteragirComJanelasPopUpSemIdentificador() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
	
		driver.get(url);		
		
		driver.manage().window().maximize();
		
		driver.findElement(By.id("buttonPopUpHard")).click();
		
		System.out.println("driver.getWindowHandle(): " + driver.getWindowHandle());

		System.out.println("driver.getWindowHandles(): " + driver.getWindowHandles());	
		
		driver.switchTo().window(driver.getWindowHandles().toArray()[1].toString());
		
		String textoDeSuaEscolha = "Lorem ipsulummmmm";
		
		driver.findElement(By.tagName("textarea")).sendKeys(textoDeSuaEscolha);	
		
		String textoDoPopUp = "";
		
		textoDoPopUp = driver.findElement(By.tagName("textarea")).getAttribute("value");
		
		System.out.println("textoDoPopUp: " + textoDoPopUp);
		
		assertEquals(textoDoPopUp, textoDeSuaEscolha);
		
		driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
		
		driver.findElement(By.id("elementosForm:sugestoes")).sendKeys(textoDeSuaEscolha);	
		
		String textoCampoSugestoes = "";
		
		textoCampoSugestoes = driver.findElement(By.id("elementosForm:sugestoes")).getAttribute("value");
		
		System.out.println("textoCampoSugestoes: " + textoCampoSugestoes);
		
		assertEquals(textoCampoSugestoes, textoDeSuaEscolha);
		
		driver.quit();
		
	}
	
}
