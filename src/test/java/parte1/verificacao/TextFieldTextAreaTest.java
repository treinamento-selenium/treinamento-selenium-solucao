package parte1.verificacao;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TextFieldTextAreaTest {

	@Test
	public void devePreencherTextFieldTextArea() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
	
		driver.get(url);			
		
		driver.manage().window().maximize();
		
		driver.findElement(By.id("elementosForm:nome")).sendKeys("Roger");	
		
		String textoPreenchidoNoCampoNome = "";
		
		textoPreenchidoNoCampoNome = driver.findElement(By.id("elementosForm:nome")).getAttribute("value");
		
		System.out.println("textoPreenchidoNoCampoNome: " + textoPreenchidoNoCampoNome);
		
		assertEquals(textoPreenchidoNoCampoNome, "Roger");
		
		driver.findElement(By.id("elementosForm:sugestoes")).sendKeys("Descreva uma sugest�o");
		
		String textoPreenchidoNoCampoSugestoes = "";
		
		textoPreenchidoNoCampoSugestoes = driver.findElement(By.id("elementosForm:sugestoes")).getAttribute("value");
		
		System.out.println("textoPreenchidoNoCampoSugestoes: " + textoPreenchidoNoCampoSugestoes);
		
		assertEquals(textoPreenchidoNoCampoSugestoes, "Descreva uma sugest�o");
		
		driver.close();
		
	}
	
}
