package parte1.interacao;

import static org.testng.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class CapturaDeTextoMaPraticaTest {

	@Test
	public void deveCapturarTexto() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
	
		driver.get(url);		
		
		driver.manage().window().maximize();
		
		String codigoFonte = "";
		
		codigoFonte = driver.getPageSource();
		
		System.out.println("codigoFonte: " + codigoFonte);
		
		String textoTagBody = "";
		
		textoTagBody = driver.findElement(By.tagName("body")).getText();
		
		System.out.println("textoTagBody: " + textoTagBody );
		
		String tituloPagina = "";
		
		tituloPagina = driver.getTitle();
		
		System.out.println("tituloPagina: " + tituloPagina);
		
		driver.close();
		
		fail("Teste ainda n�o est� completo!");
		
	}
	
}
