package parte2.frames;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class FramesTest {

	@Test
	public void deveInteragirComFrame() {
		
		String driverPath = "C:\\selenium\\drivers\\chrome\\x32_2.46\\chromedriver.exe";			
		
		System.setProperty("webdriver.chrome.driver", driverPath);	
		
		WebDriver driver = new ChromeDriver();
		
		String url = "file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html";
	
		driver.get(url);		
		
		driver.manage().window().maximize();
		
		driver.switchTo().frame("frame1");
				
		driver.findElement(By.id("frameButton")).click();
		
		String mensagemNoAlerta = "";
		
		Alert alert = driver.switchTo().alert();
		
		mensagemNoAlerta = alert.getText();
		
		System.out.println("mensagemNoAlerta: " + mensagemNoAlerta);	
		
		alert.accept();
		
		assertEquals(mensagemNoAlerta, "Frame OK!");
		
		driver.switchTo().defaultContent();
		
		driver.close();		
		
	}
	
}
